﻿using Base.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace Base.DAL.Repositories
{
    public class MovieRepository : BaseRepository<Movie>
    {
        public MovieRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }

        public IEnumerable<Movie> GetByCategory(int catId)
        {
            throw new NotImplementedException();
        }
    }
}
