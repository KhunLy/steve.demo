﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace Base.ASP.DI.Services
{
    public class TokenService
    {
        private JwtHeader _header;

        private JwtSecurityTokenHandler _handler;

        private SecurityKey _key;
        public TokenService(string signature)
        {
            _key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signature));
            _header = new JwtHeader(new SigningCredentials(_key, SecurityAlgorithms.HmacSha512));
            _handler = new JwtSecurityTokenHandler();
        }

        public string GenerateToken(string username, string role, string id)
        {
            JwtPayload payload = new JwtPayload();
            payload.Add("username", username);
            payload.Add("role", role);
            payload.Add("id", id);
            JwtSecurityToken token = new JwtSecurityToken(_header, payload);
            return _handler.WriteToken(token);
        } 

        public ClaimsPrincipal Validate(string token)
        {
            return _handler.ValidateToken(
                token, 
                new TokenValidationParameters() { IssuerSigningKey = _key }, 
                out SecurityToken securityToken
            );
        }
    }
}