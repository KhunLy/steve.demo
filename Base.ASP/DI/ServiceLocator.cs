﻿using Base.ASP.DI.Services;
using Base.DAL.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Base.ASP.DI
{
    public static class ServiceLocator
    {
        private static ServiceCollection _collection;
        private static ServiceProvider _provider;

        static ServiceLocator()
        {
            string conString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string providerName = ConfigurationManager.ConnectionStrings["default"].ProviderName;
            _collection = new ServiceCollection();
            _collection.AddTransient(x => new CategoryRepository(conString, providerName));
            _collection.AddTransient(x => new MovieRepository(conString, providerName));
            _collection.AddTransient(x => new TokenService(ConfigurationManager.AppSettings["ApiSignature"]));
            _provider = _collection.BuildServiceProvider();
        }

        public static T Get<T>()
        {
            return _provider.GetService<T>();
        }
    }
}