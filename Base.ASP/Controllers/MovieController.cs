﻿using Base.ASP.DI;
using Base.ASP.DTO;
using Base.ASP.Filters;
using Base.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ToolBox.Mapper;

namespace Base.ASP.Controllers
{
    public class MovieController : ApiController
    {
        [HttpGet]
        [ApiAuthorize("ADMIN")]
        public IEnumerable<MovieDTO> Get([FromUri]int limit, [FromUri]int offset)
        {
            return ServiceLocator.Get<MovieRepository>().Get(limit, offset)
                .Select(x => x.MapTo<MovieDTO>());
        }
    }
}
