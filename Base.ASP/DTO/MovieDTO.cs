﻿using Base.ASP.DI;
using Base.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Base.ASP.DTO
{
    public class MovieDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int CategoryId { get; set; }

        private string _categoryName;
        public string CategoryName 
        { 
            get 
            {
                return _categoryName = _categoryName ??
                    ServiceLocator.Get<CategoryRepository>().GetById(CategoryId)?.Name;
            } 
        }
    }
}