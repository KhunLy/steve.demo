﻿using Base.ASP.DI;
using Base.ASP.DI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Base.ASP.Filters
{
    public class ApiAuthorizeAttribute: AuthorizeAttribute
    {
        private string[] _roles;

        public ApiAuthorizeAttribute(params string[] roles)
        {
            _roles = roles;
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            actionContext.Request.Headers.TryGetValues(
                "Authorization", out IEnumerable<string> values
            );
            string token 
                = values?.FirstOrDefault(a => a.StartsWith("Bearer "))?.Replace("Bearer ", "");
            if (token is null)
            {
                throw new HttpResponseException(System.Net.HttpStatusCode.Unauthorized);
            }
            ClaimsPrincipal claims = ServiceLocator.Get<TokenService>().Validate(token);
            if (claims is null)
            {
                throw new HttpResponseException(System.Net.HttpStatusCode.Unauthorized);
            }
            else
            {
                string role = claims.FindFirst("role")?.ToString();
                if (!_roles.Contains(role))
                {
                    throw new HttpResponseException(System.Net.HttpStatusCode.Unauthorized);
                }
            }
        }
    }
}