﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ToolBox.Mapper
{
    public static class MapperExtension
    {
        public static T MapTo<T>(this object from)
            where T: new()
        { 
            T result = new T();
            var resultProperties = typeof(T).GetProperties();
            foreach(PropertyInfo resultProperty in resultProperties)
            {
                PropertyInfo fromProperty
                    = from.GetType().GetProperty(resultProperty.Name);
                if(fromProperty != null)
                {
                    object valueToSet = fromProperty.GetValue(from);
                    resultProperty.SetValue(result, valueToSet);
                }
            }
            return result;
        }
    }
}
